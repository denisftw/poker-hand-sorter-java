## Building and running the application

This application is tested on JDK 15, which can be downloaded from [here](https://adoptopenjdk.net/index.html?variant=openjdk16&jvmVariant=hotspot). Provided that JDK15 or later is installed, it can be compiled using the Gradle wrapper that accompanies the source code distribution.

```text
$ ./gradlew shadowJar
```

The above command builds a JAR file containing everything needed to run the application. To run it, simply type the following:

```text
$ java -jar build/libs/poker-hand-sorter-0.0.1-SNAPSHOT-all.jar poker-hands.txt
```

As illustrated above, the file containing poker hands must be supplied as the program argument.