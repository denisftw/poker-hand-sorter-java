package com.appliedscala.challenges.poker;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.function.Function;

enum CardRank {
    PIP2('2', 2),
    PIP3('3', 3),
    PIP4('4', 4),
    PIP5('5', 5),
    PIP6('6', 6),
    PIP7('7', 7),
    PIP8('8', 8),
    PIP9('9', 9),
    PIP10('T', 10),
    JACK('J', 11),
    QUEEN('Q', 12),
    KING('K', 13),
    ACE('A', 14),
    ;
    private final char abbreviation;
    private final int value;
    private static final Map<Character, CardRank> conversion = List.ofAll(EnumSet.allOf(CardRank.class)
            .stream()).toMap(CardRank::getAbbreviation, Function.identity());

    public static CardRank fromText(char text) {
        return conversion.getOrElse(text, null);
    }

    CardRank(char abbreviation, int value) {
        this.abbreviation = abbreviation;
        this.value = value;
    }

    private char getAbbreviation() {
        return abbreviation;
    }

    public int getValue() {
        return value;
    }
}

enum CardSuit {
    DIAMONDS('D'),
    HEARTS('H'),
    SPADES('S'),
    CLUBS('C'),
    ;
    private final char abbreviation;
    private static final Map<Character, CardSuit> conversion = List.ofAll(EnumSet.allOf(CardSuit.class)
            .stream()).toMap(CardSuit::getAbbreviation, Function.identity());

    public static CardSuit fromText(char text) {
        return conversion.getOrElse(text, null);
    }

    CardSuit(char abbreviation) {
        this.abbreviation = abbreviation;
    }

    private char getAbbreviation() {
        return abbreviation;
    }
}
class PlayingCard {
    private final CardRank rank;
    private final CardSuit suit;

    public PlayingCard(CardRank rank, CardSuit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public static PlayingCard fromText(String text) {
        return new PlayingCard(CardRank.fromText(text.charAt(0)), CardSuit.fromText(text.charAt(1)));
    }

    public CardRank getRank() {
        return rank;
    }

    public CardSuit getSuit() {
        return suit;
    }
}

class PokerHand {
    private final List<PlayingCard> cards;
    public PokerHand(List<PlayingCard> cards) {
        assert cards.size() == 5;
        this.cards = cards;
    }

    public List<PlayingCard> getCards() {
        return cards;
    }
}

class CombinationWithRanks {
    private final HandCombination combination;
    private final List<CardRank> cardRanks;

    public CombinationWithRanks(HandCombination combination, List<CardRank> cardRanks) {
        this.combination = combination;
        this.cardRanks = cardRanks;
    }

    public static final Comparator<CombinationWithRanks> COMPARATOR =
            Comparator.<CombinationWithRanks, Integer>comparing(o -> o.combination.getCombinationRank()).
                    thenComparing(c -> c.cardRanks, PokerComparators.lists());

    public HandCombination getCombination() {
        return combination;
    }

    public List<CardRank> getCardRanks() {
        return cardRanks;
    }
}

class PokerComparators {
    public static <T> Comparator<T> desc(Function<T, Integer> extractor) {
        return Comparator.comparing(extractor, Comparator.reverseOrder());
    }

    public static Comparator<PlayingCard> cardDesc() {
        return Comparator.comparing(c -> c.getRank().getValue(), Comparator.reverseOrder());
    }

    public static Comparator<List<CardRank>> lists() {
        return (o1, o2) -> {
            for (var tuple : o1.zip(o2)) {
                if (tuple._1.getValue() < tuple._2.getValue()) {
                    return -1;
                } else if (tuple._1.getValue() > tuple._2.getValue()) {
                    return 1;
                }
            }
            return 0;
        };
    }

    public static List<CardRank> toSortedRanks(List<PlayingCard> cards) {
        return cards.sorted(cardDesc()).map(PlayingCard::getRank);
    }
}

enum HandCombination {
    HIGH_CARD(1, HandCombination::findHighCard),
    PAIR(2, HandCombination::findPair),
    TWO_PAIRS(3, HandCombination::findTwoPairs),
    THREE_OF_A_KIND(4, HandCombination::findThreeOfAKind),
    STRAIGHT(5, HandCombination::findStraight),
    FLUSH(6, HandCombination::findFlush),
    FULL_HOUSE(7, HandCombination::findFullHouse),
    FOUR_OF_A_KIND(8, HandCombination::findFourOfAKind),
    STRAIGHT_FLUSH(9, HandCombination::findStraightFlush),
    ROYAL_FLUSH(10, HandCombination::findRoyalFlush),
    ;

    private final Function<PokerHand, List<CardRank>> findBestCombination;
    private final int combinationRank;
    private final static List<HandCombination> nonFallbackCombinations =
            List.ofAll(EnumSet.allOf(HandCombination.class).stream().filter(s -> s != HIGH_CARD))
            .sorted(Comparator.comparingInt(HandCombination::getCombinationRank).reversed());

    HandCombination(int combinationRank, Function<PokerHand, List<CardRank>> findBestCombination) {
        this.findBestCombination = findBestCombination;
        this.combinationRank = combinationRank;
    }

    public static CombinationWithRanks fromHand(PokerHand hand) {
        for (var combination: nonFallbackCombinations) {
            var maybeRanks = combination.findBestCombination.apply(hand);
            if (maybeRanks.nonEmpty()) {
                return new CombinationWithRanks(combination, maybeRanks);
            }
        }
        // Fallback to HIGH_CARD
        return new CombinationWithRanks(HIGH_CARD, HIGH_CARD.findBestCombination.apply(hand));
    }

    public int getCombinationRank() {
        return combinationRank;
    }

    private static List<PlayingCard> sortedByRank(List<PlayingCard> cards) {
        return cards.sorted(Comparator.comparingInt(card -> -card.getRank().getValue()));
    }

    private static Map<CardRank, List<PlayingCard>> groupedByRank(PokerHand hand) {
        return hand.getCards().groupBy(PlayingCard::getRank);
    }

    private static Map<CardSuit, List<PlayingCard>> groupedBySuit(PokerHand hand) {
        return hand.getCards().groupBy(PlayingCard::getSuit);
    }

    private static List<CardRank> findNOfAKind(PokerHand hand, int count) {
        final var grouped = groupedByRank(hand);
        final var pairs = grouped.iterator().filter(entry -> entry._2.size() == count).toList()
                .sorted(PokerComparators.desc(t -> t._2.length()));
        if (pairs.nonEmpty()) {
            final var combinationCards = pairs.head()._2.toSet();
            final var remainingCards = hand.getCards().
                    filter(c -> !combinationCards.contains(c)).sorted(PokerComparators.cardDesc());
            return pairs.head()._2.appendAll(remainingCards).map(PlayingCard::getRank);
        } else {
            return List.empty();
        }

    }

    private static List<CardRank> findHighCard(PokerHand hand) {
        final var cards = sortedByRank(hand.getCards());
        return cards.map(PlayingCard::getRank);
    }

    private static List<CardRank> findPair(PokerHand hand) {
        return findNOfAKind(hand, 2);
    }

    private static List<CardRank> findThreeOfAKind(PokerHand hand) {
        return findNOfAKind(hand, 3);
    }

    private static List<CardRank> findFourOfAKind(PokerHand hand) {
        return findNOfAKind(hand, 4);
    }

    private static List<CardRank> findFlush(PokerHand hand) {
        final var grouped = groupedBySuit(hand);
        final var maybeFound = grouped.values().find(playingCards -> playingCards.size() == 5);
        if (maybeFound.isDefined()) {
            return PokerComparators.toSortedRanks(hand.getCards());
        } else return List.empty();
    }

    private static List<Tuple2<CardRank, List<PlayingCard>>> groupAndSortEntries(PokerHand hand) {
        final var byRank = groupedByRank(hand);
        List<Tuple2<CardRank, List<PlayingCard>>> entries = byRank.iterator().toList().
                sorted(PokerComparators.desc(entry -> entry._2.size()));
        return entries;
    }

    private static List<CardRank> findStraight(PokerHand hand) {
        final var grouped = groupAndSortEntries(hand);
        if (grouped.size() == 5 && (grouped.get(0)._1.getValue() - grouped.get(4)._1.getValue() == 4)) {
            return PokerComparators.toSortedRanks(hand.getCards());
        } else return List.empty();
    }

    private static List<CardRank> findTwoPairs(PokerHand hand) {
        final var entries = groupAndSortEntries(hand);
        if (entries.size() == 3 && entries.get(0)._2.size() == 2 && entries.get(1)._2.size() == 2) {
            final var combinationRanks = List.of(entries.get(0)._1, entries.get(1)._1)
                    .sorted(PokerComparators.desc(CardRank::getValue));
            final var remainingRank = entries.get(2)._1;
            return combinationRanks.append(remainingRank);
        } else return List.empty();
    }

    private static List<CardRank> findFullHouse(PokerHand hand) {
        final var entries = groupAndSortEntries(hand);
        if (entries.size() == 2) {
            return List.of(entries.get(0)._1, entries.get(1)._1)
                    .sorted(PokerComparators.desc(CardRank::getValue));
        } else return List.empty();
    }

    private static List<CardRank> findStraightFlush(PokerHand hand) {
        final var maybeFlush = findFlush(hand);
        if (maybeFlush.nonEmpty()) {
            return findStraight(hand);
        } else return List.empty();
    }

    private static List<CardRank> findRoyalFlush(PokerHand hand) {
        final var maybeFlush = findFlush(hand);
        if (maybeFlush.nonEmpty() && maybeFlush.contains(CardRank.ACE)) {
            return findStraight(hand);
        } else return List.empty();
    }
}

class PokerGame {
    private final PokerHand player1;
    private final PokerHand player2;

    public PokerGame(PokerHand player1, PokerHand player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public static PokerGame fromText(String bothHands) {
        final var cards = List.ofAll(Arrays.stream(bothHands.split(" "))).map(PlayingCard::fromText);
        return new PokerGame(new PokerHand(cards.slice(0, 5)), new PokerHand(cards.slice(5, 10)));
    }
    public PokerHand getPlayer1() {
        return player1;
    }
    public PokerHand getPlayer2() {
        return player2;
    }
}

public class PokerHandSorter {
    public static final Logger log = LoggerFactory.getLogger(PokerHandSorter.class);

    public Flux<String> sortHands(Flux<String> encodedHands) {
        final var games = encodedHands.map(PokerGame::fromText);
        final var winners = games.map(game -> {
            final var combination1 = HandCombination.fromHand(game.getPlayer1());
            final var combination2 = HandCombination.fromHand(game.getPlayer2());
            return (CombinationWithRanks.COMPARATOR.compare(combination1, combination2) < 0) ? 2 : 1;
        });
        return winners.groupBy(Function.identity()).flatMap(grouped -> {
            return grouped.count().map(count -> "Player " + grouped.key() + ": " + count);
        });
    }

    private static Flux<String> readFileContents(String filePath) {
        return Flux.create(sink -> {
            try (BufferedReader reader = Files.newBufferedReader(Path.of(filePath))) {
                while (reader.ready() && !sink.isCancelled()) {
                    sink.next(reader.readLine());
                }
                sink.complete();
            } catch (IOException ioe) {
                sink.error(ioe);
            }
        });
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: program <file>");
        } else {
            final var pokerHands = readFileContents(args[0].trim());
            final var sorter = new PokerHandSorter();
            sorter.sortHands(pokerHands).subscribe(System.out::println);
        }
    }
}
