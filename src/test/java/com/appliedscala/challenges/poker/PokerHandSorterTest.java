package com.appliedscala.challenges.poker;

import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PokerHandSorterTest {
    @Test
    public void testFindingCorrectCombinations() {
        final var bothHands = "4H 7C 4S 8D 8C TH TS QH QS QD";
        final var pokerGame = PokerGame.fromText(bothHands);
        final var player1Combination = HandCombination.fromHand(pokerGame.getPlayer1());
        final var player2Combination = HandCombination.fromHand(pokerGame.getPlayer2());
        assertEquals(HandCombination.TWO_PAIRS, player1Combination.getCombination());
        assertEquals(List.of(CardRank.PIP8, CardRank.PIP4, CardRank.PIP7), player1Combination.getCardRanks());
        assertEquals(HandCombination.FULL_HOUSE, player2Combination.getCombination());
        assertEquals(List.of(CardRank.QUEEN, CardRank.PIP10), player2Combination.getCardRanks());
        final var winner = CombinationWithRanks.COMPARATOR.compare(player1Combination, player2Combination);
        assertEquals(-1, winner);
    }
}
